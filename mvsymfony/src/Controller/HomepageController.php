<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 7. 3. 2019
 * Time: 19:15
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(){
        return $this->render('front/homepage.php.twig');
    }
}