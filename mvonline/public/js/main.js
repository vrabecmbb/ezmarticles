$(function () {

    $

    var window_width = $(window).width();

    //calculate #content height
    var change_content_height = function(){
      var winheight = $(window).height();
      var content_height = winheight - $("#header").outerHeight() - $("footer").outerHeight();
      $("#content").css({'min-height': content_height});
    };

    //cahnge articles images on small screen
    var change_articles_images = function(){
      var article_container_width = $(".single .article").width();
      var images = $(".single .article img");
      images.each(function(i){
        var ratio = $(this).width() / $(this).height();

        if ($(this).width() > article_container_width){
          var new_width = article_container_width;
          var new_height = new_width / ratio;
          $(this).css({width: new_width, height: new_height});
        }
      });
    };

    change_content_height();
    change_articles_images();

    $(window).on("orientationchange", function(){
      change_articles_images();
    });

    $(window).resize(function(){
      change_content_height();
      change_articles_images();
    });

    //toggling sticky menu
    //$('#secondary-menu-wrapper').show();

    $(window).scroll(function(){
        var sticky_menu = $('#secondary-menu-wrapper');

        if ($(window).scrollTop() >= 110){
            sticky_menu.slideDown(300);
        } else {
            sticky_menu.slideUp(100);
        }
    });

    //admin geocaching grid
    $('.gc-edit').on('click', function (event) {
        var id = $(this).attr('data-id');
        $.ajax({
            url: '/admin/geocaching/default/' + id + '?do=fullForm',
            data: '',
            success: function (e) {
                var content = e.snippets['snippet--form'];
                $('#snippet--form').html(content);
                $('#myModal').modal('show');
            }
        });

        event.preventDefault();
    });
    
    $('body').on('click','.gcEditSendBtn', function(){
    });
    

    //diary search
    var xhr = null;
    var $spinner = $('.spinner');
    $('.form-search ').on('keyup', function () {
	     var inputSearch = $(this).find($('input[name="search"]'));
	     var inputSearchType = $(this).find($('input[name="searchType"]'));

	    var searchVal = inputSearch.val();
	    var searchTypeVal = inputSearchType.val();

	    var link = inputSearch.data('link').replace('__REPLACEME__', searchVal);
	    link = link.replace('__REPLACETYPE__', searchTypeVal);

    	if (searchVal.length >= 0) {
    		$spinner.show();
    		if (xhr !== null) {
    			xhr.abort();
    		}
    		xhr = $.post(link, function (payload) {
    			$spinner.hide();
    			//jQuery.nette.success(payload);
        });
    	}
    });


    $(document).on('click', '#frm-geocachingEditForm-form-save', function () {

        $('.spinner').show();

        var region = $('#frm-geocachingEditForm-form-region').val();
        var district = $('#frm-geocachingEditForm-form-district').val();
        var town = $('#frm-geocachingEditForm-form-town').val();
        var id = $('#frm-geocachingEditForm-form-id').val();
        var date = $('#datetimepicker').val();
        var hour = $('#frm-geocachingEditForm-form-hour').val();
        var minutes = $('#frm-geocachingEditForm-form-minutes').val();

        //?region=&district=&town=&save=Uložiť&do=geocachingEditForm-form-submit&save=Uložiť&do=geocachingEditForm-form-submit

        //$.get('/www/admin/geocaching/?name='+region+'&save=Uložiť&do=geocachingEditForm-form-submit');
        $.ajax({
            url: '/www/admin/geocaching',
            data: 'region='+region+'&town='+town+'&district='+district+'&id='+id+'&date='+date+'&hour='+hour+'&minutes='+minutes+'&save=Uložiť&do=geocachingEditForm-form-submit',
            type: 'POST',
            success: function(payload){
                if (payload.redirect) {
                    window.location.href = payload.redirect;
                }
            }
        });
    });

    //ajax-form spinner
    /*$.nette.ext('spinner', {
        init: function () {
            var spinner = $(".spinner");
        },
        before: function (settings, ui, e) {
            $(".spinner").show();
        },
        complete: function () {
            $(".spinner").hide();
        }
    });*/

    //init ajax
    //$.nette.init();

    //flash messages effects
    $('.flash').delay(10000).slideToggle(1500);
    
    //li-searcher
    $('li.li-searcher').click(function(){
        $('ul.main-menu').find('li:not(:last-child)').hide();
        $('li.li-searcher-input').removeClass('hidden');
    });
    
    $('li.li-searcher-input form').submit(function(e){
        e.preventDefault();
        
        var search = $('li.li-searcher-input form input').val();
        window.location.href = 'http://mvonline.eu/diaries/search/'+search;
    });
    
    /*$('li.li-searcher').on('keypress', function (e) {
         if(e.which === 13){
            console.log('asfasfasf');
         }
   });*/

    //geocaching-processing ajax
    $("form.geocachingProcessForm").submit(function (event) {
        //disable the default form submission
        event.preventDefault();

        //grab all form data
        var formData = new FormData($(this)[0]);

        $.ajax({
            async: false,
            url: '',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#spinner').show();
            },
            success: function (payload) {
                if (payload.redirect) {
                    window.location.href = payload.redirect;
                }
                $('#spinner').hide();
            }
        });
        return false;
    });
});
