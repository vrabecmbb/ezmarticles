<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 29. 10. 2018
 * Time: 7:35
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Geocaching_countries
 *
 * @author Administrator
 * @ORM\Entity
 * @ORM\Table(name="geocaching_countries")
 */
class GeocachingCountries
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\OneToMany(targetEntity="Geocaching", mappedBy="country")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column (type="string")
     */
    public $country;

    /**
     * @ORM\Column (type="string")
     */
    public $originalName;

    /**
     * @ORM\Column (type="string")
     */
    public $image;

}