<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 29. 10. 2018
 * Time: 10:17
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * Description of Geocaching_countries
 *
 * @ORM\Entity
 * @ORM\Table(name="admin")
 */
class Users implements Authenticatable
{

    use \LaravelDoctrine\ORM\Auth\Authenticatable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $admin_id;

    /**
     * @ORM\Column (type="text",)
     */
    protected $login;

    public function getAuthIdentifierName()
    {
        return 'id';
    }

    public function getAuthIdentifier()
    {
        return $this->admin_id;
    }

    public function getPassword()
    {
        return $this->password;
    }
}