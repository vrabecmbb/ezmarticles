<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 29. 10. 2018
 * Time: 7:36
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Geocaching_countries
 *
 * @ORM\Entity
 * @ORM\Table(name="geocaching_types")
 */

class GeocachingTypes
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(type="string")
     */
    public $idName;

    /**
     * @ORM\Column (type="string")
     */
    public $name;

    /**
     * @ORM\Column (type="integer")
     */
    public $position;

    /**
     * @ORM\Column (type="string")
     */
    public $image;

    /**
     * @ORM\Column (type="string")
     */
    public $color;
}