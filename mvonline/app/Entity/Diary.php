<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/** All entity classes must be declared as such.
 *
 * @ORM\Entity(repositoryClass="App\Managers\Repositories\DiaryRepository")
 * @ORM\Table(name="diary")
 */
class Diary
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(name="title")
     */
    public $title;

    /**
     * @ORM\Column (type="datetime")
     */
    public $fromDate;

    /**
     * @ORM\Column (type="datetime", nullable=true)
     */
    public $toDate;

    /**
     * @ORM\Column (type="text",)
     */
    public $text;

    /**
     * @ORM\Column (type="text", nullable=true)
     */
    public $photogallery;

    /**
     * @ORM\Column (type="text", nullable=true)
     */
    public $keywords;
}