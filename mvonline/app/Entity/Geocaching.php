<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * All entity classes must be declared as such.
 *
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Managers\Repositories\GeocachingRepository")
 * @ORM\Table(name="geocaching")
 */
class Geocaching{

    /**
     * @ORM\GeneratedValue
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    public $number;

    /**
     * @ORM\Column(type="string")
     */
    public $id;

    /**
     * @ORM\ManyToOne(targetEntity="GeocachingTypes")
     * @ORM\JoinColumn(name="type")
     */
    public $type;

    /**
     * @ORM\Column (type="string")
     */
    public $name;

    /**
     * @ORM\Column (type="string")
     */
    public $owner;

    /**
     * @ORM\Column (type="float")
     */
    public $lon;

    /**
     * @ORM\Column (type="float")
     */
    public $lat;

    /**
     * @ORM\ManyToOne(targetEntity="GeocachingCountries")
     * @ORM\JoinColumn(name="country")
     */
    public $country;

    /**
     * @ORM\Column (type="string")
     */
    public $region;

    /**
     * @ORM\Column (type="string",nullable=TRUE)
     */
    public $district = "";

    /**
     * @ORM\Column (type="string",nullable=TRUE)
     */
    public $town;

    /**
     * @ORM\Column (type="integer", nullable=TRUE)
     */
    public $altitude = 0;

    /**
     * @ORM\Column (type="datetime")
     */
    public $found;

    /**
     * @ORM\Column (type="float")
     */
    public $difficulty;

    /**
     * @ORM\Column (type="float")
     */
    public $terrain;

    /**
     * @ORM\Column (type="boolean")
     */
    public $processedGeoCode = FALSE;

    /**
     * @ORM\Column (type="boolean")
     */
    public $processedElevation = FALSE;

}
