<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 8. 11. 2018
 * Time: 14:17
 */

namespace App\Managers\Repositories;

use App\Entity\Geocaching;
use Doctrine\ORM\EntityRepository;
use LaravelDoctrine\ORM\Pagination\PaginatesFromRequest;

class GeocachingRepository extends EntityRepository
{
    use PaginatesFromRequest;

    public function getAllPage($filter){
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select(['gc', 'c', 't'])
            ->from(Geocaching::class, 'gc')
            ->leftJoin('gc.country', 'c')
            ->leftJoin('gc.type', 't')
            //->innerJoin('gc.type', 'b')//, array('country' => 'c.country', 'type' => 'b.name'))
            //->
            ->orderBy('gc.number', 'DESC')
        ;

        if ($filter){
            $orderBy = sprintf('gc.%s',$filter['order_by']);
            $orderDir = $filter['order_dir'];

            $qb->orderBy($orderBy,$orderDir);
        }

        return $this->paginate($qb->getQuery(), 10, 'page');
    }
}