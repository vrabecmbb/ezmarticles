<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 8. 11. 2018
 * Time: 14:17
 */

namespace App\Managers\Repositories;


use App\Entity\Diary;
use Doctrine\ORM\EntityRepository;
use LaravelDoctrine\ORM\Pagination\PaginatesFromRequest;

class DiaryRepository extends EntityRepository
{
    use PaginatesFromRequest;

    public function getAllPage($filter){
        $qb = $this->_em->createQueryBuilder()
            ->select('a')
            ->from(Diary::class,'a')
            //->orderBy('a.id ','DESC')
        ;

        if ($filter){
            $orderBy = sprintf('a.%s',$filter['order_by']);
            $orderDir = $filter['order_dir'];

            $qb->orderBy($orderBy,$orderDir);
        }

        return $this->paginate($qb->getQuery(), 10, 'page');
    }
}