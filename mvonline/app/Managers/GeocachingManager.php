<?php

namespace App\Managers;

use App\Entity\Geocaching;
use Doctrine\ORM\EntityManager;

class GeocachingManager
{

    /**
     * @var string
     */
    private $class = Geocaching::class;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * GeocachingManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getNewest($limit = 5)
    {

        $qb = $this->em->createQueryBuilder();
        $qb
            ->select(['gc', 'c', 't'])
            ->from(Geocaching::class, 'gc')
            ->leftJoin('gc.country', 'c')
            ->leftJoin('gc.type', 't')
            //->innerJoin('gc.type', 'b')//, array('country' => 'c.country', 'type' => 'b.name'))
            //->
            ->orderBy('gc.found', 'DESC')
            ->setMaxResults($limit);


        return $qb->getQuery()->getResult();
    }

    public function getById($id){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select(['gc','c','t'])
            ->from(Geocaching::class,'gc')
            ->leftJoin('gc.country','c')
            ->leftJoin('gc.type','t')
            ->where('gc.id=:id')
            ->setParameter('id',$id);

        return $qb->getQuery()->getSingleResult();
    }

    public function updateGeocaching($geocache){
        $this->em->persist($geocache);
        $this->em->flush();

        return $this;
    }


}