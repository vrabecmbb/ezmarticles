<?php

namespace App\Managers;

class Dates{

    const SK = "d.m.Y";

    public function toFormatedString($date,$format){
        if (!is_null($date)){
            if ($date instanceof \DateTime){
                return date($format, $date->getTimestamp());
            }
            return date($format, $date);
        }
        return NULL;
    }
}