<?php

namespace App\Managers;

use App\Entity\Diary;
use Doctrine\ORM\EntityManager;

class DiaryManager
{

    /**
     * @var string
     */
    private $class = 'App\Entity\Diary';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * DiaryManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getAll()
    {
        $diaries = $this->em->getRepository(Diary::class)->findAll();

        return $diaries;
    }

    public function get($id)
    {
        return $this->em->find(Diary::class, $id);
    }

    public function getArray($id){
        $qb = $this->em->createQueryBuilder();

        $qb->select('r')
            ->from(Diary::class,'r')
            ->where('r.id=:id')
            ->setParameter('id',$id);

        $ret = $qb->getQuery()->getArrayResult();

        return reset($ret);
    }

    public function getLimited($limit)
    {

        $qb = $this->em->createQueryBuilder();
        $qb->addSelect('d')
            ->from(Diary::class, 'd')
            ->setMaxResults($limit)
            ->orderBy('d.fromDate', 'DESC');

        return $qb->getQuery()->getResult();

    }

    public function insert($diary)
    {
        $this->em->persist($diary);
        $this->em->flush();

        return $diary->id;
    }

    /**
     * Aktualizacia zapisu dennicka
     * @param type $data
     */
    public function update($data)
    {
        $this->em->flush();
        //$this->em->merge($data);
        $this->em->flush();
        return $this;
    }

    public function addPhotogallery($id, $photogallery)
    {
        $diary = $this->em->find(Diary::class, $id);
        $diary->photogallery = $photogallery;
        $this->em->persist($diary);
        $this->em->flush();
    }

}