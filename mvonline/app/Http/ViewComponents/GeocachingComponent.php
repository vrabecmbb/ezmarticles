<?php

namespace App\Http\ViewComponents;

use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Htmlable;

class GeocachingComponent implements Htmlable
{
    /** \Illuminate\Http\Request */
    private $request;

    /** @var string */
    private $backgroundColor;

    public function __construct(Request $request, string $backgroundColor)
    {
        $this->request = $request;
        $this->backgroundColor = $backgroundColor;
    }

    public function toHtml(): string
    {
        return view('components.geocachingFilter', [
            'activeUrl' => $this->request->url(),
            'backgroundColor' => $this->backgroundColor,
        ]);
    }
}