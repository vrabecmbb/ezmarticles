<?php

namespace App\Http\Controllers;

use App\Model\FormErrors;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var EntityManagerInterface
     */
    public $em;

    /**
     * @var FormErrors
     */
    public $formErrors;

    /**
     * Controller constructor.
     * @param Application $app
     * @param EntityManagerInterface|NULL $em
     * @param FormErrors $formErrors
     * @throws \ReflectionException
     */
    public function __construct(Application $app, EntityManagerInterface $em = NULL, FormErrors $formErrors = NULL)
    {
        static $dependencies;

        if ($dependencies == NULL){
            $reflector = new \ReflectionClass(__CLASS__);
            $constructor = $reflector->getConstructor();
            $dependencies = $constructor->getParameters();
        }

        foreach ($dependencies as $dependency){
            if (${$dependency->name} === NULL){
                    // Assign variable
                ${$dependency->name} = $app->make($dependency->getClass()->name);
            }
        }

        $this->em = $em;
        $this->formErrors = $formErrors->getMessages();
    }

    protected function getClassName(){
        return get_called_class();
    }

    protected function fireCallbacks($callbacks, $args = [])
    {
        foreach ($callbacks as $callback) {
            call_user_func_array($callback, $args);
        }
    }
}
