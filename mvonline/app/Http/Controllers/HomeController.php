<?php

namespace App\Http\Controllers;

use App\Managers\DiaryManager;
use App\Managers\GeocachingManager;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var DiaryManager
     */
    private $diaryManager;

    /**
     * @var GeocachingManager
     */
    private $geocachingManager;

    /**
     * HomeController constructor.
     * @param DiaryManager $diaryManager
     * @param GeocachingManager $geocachingManager
     */
    public function __construct(DiaryManager $diaryManager, GeocachingManager $geocachingManager)
    {
        $this->diaryManager = $diaryManager;
        $this->geocachingManager = $geocachingManager;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diaries = $this->diaryManager->getLimited(5);
        $geocaching = $this->geocachingManager->getNewest(8);

        return view('pages.index',[
            'diaries' => $diaries,
            'geocaching' => $geocaching
        ]);
    }
}
