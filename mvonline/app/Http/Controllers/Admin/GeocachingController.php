<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 30. 11. 2018
 * Time: 7:38
 */

namespace App\Http\Controllers\Admin;


use Aginev\Datagrid\Datagrid;
use App\Entity\Geocaching;
use App\Entity\GeocachingTypes;
use App\Http\Requests\GeocachingEditRequest;
use App\Managers\Dates;
use App\Managers\GeocachingManager;
use App\Managers\Repositories\GeocachingRepository;
use http\Env\Response;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Input;
use App\Model\FormErrors;
use Illuminate\Validation\Validator;
use Spatie\Html\Html;

class GeocachingController extends \App\Http\Controllers\Controller
{

    /** @var Dates */
    private $dates;

    /** @var GeocachingManager */
    private $geocachingManager;

    public $onGeocachingUpdate = [];

    public function __construct(Application $app, Dates $dates, FormErrors $formErrors, GeocachingManager $geocachingManager)
    {
        $this->dates = $dates;
        $this->formErrors = $formErrors;
        $this->geocachingManager = $geocachingManager;
        parent::__construct($app);
    }

    public function index()
    {
        $grid = $this->createGrid();

        return view('pages.admin.geocaching.index', [
            'grid' => $grid
        ]);

    }
    
    public function edit($id){
        $geocache = $this->geocachingManager->getById($id);
        dd($geocache);
    }

    public function ajax($id){
        $geocache = $this->geocachingManager->getById($id);

        if (!$geocache)
            return NULL;

        return response()->json([
            'geocache' => $geocache
        ]);
    }

    private function createGrid()
    {
        $default_filter = [
            "order_by" => "number",
            "order_dir" => "DESC"
        ];

        $filter = Input::get('f') ? Input::get('f') : $default_filter;

        $repository = $this->em->getRepository(Geocaching::class);

        $paginator = $repository->getAllPage($filter);

        $results = $paginator->getCollection();

        $grid = new Datagrid($results, Input::get('f', []));

        $grid
            ->setColumn('number','#')
            ->setColumn('date','D8tum',[
                'wrapper' => function ($value, $row){
                    $date = $row->getData()['found.date'];
                    $datetime = new \Datetime($date);
                    return $this->dates->toFormatedString($datetime, Dates::SK);
                }
            ])
            ->setColumn('id', 'ID', [
                'sortable' => true,
                'order' => 'DESC'
            ])
            ->setColumn('country','Krajina',[
                'wrapper' => function ($value,$row){
                $data = $row->getData();
                    return html()->img(url("images/flags/shiny/24/".$data['country.image']))->alt($data['country.country']);
                }
            ])
            ->setColumn('type','Typ',[
                'wrapper' => function($value, $row){
                $data = $row->getData();
                    return html()->img(url("images/gcTypes/".$data['type.image']))->alt($data['type.name']);
                }
            ])
            ->setColumn('name','N8zov')
            ->setColumn('region','Regi=on')
            ->setColumn('district','Okres')
            ->setColumn('town','Obec')
            ->setColumn('altitude','Vyska')

            ->setActionColumn([
                'wrapper' => function($value, $row){
                    $icon = html()
                        ->i()
                        ->class('far fa-edit')
                    ;
                    $html = html()
                        ->span()
                        ->class('fillModal')
                        ->attribute('data-toggle','modal')
                        ->attribute('data-target','#exampleModal')
                        ->attribute('data-id',$row->id)
                        ->addChild($icon)
                    ;
                    return $html;
                }
            ]);

        $grid->setPagination($paginator);

        return $grid;
    }

    public function store(\Illuminate\Http\Request $request){
        
        if($request->has('id')){
            
            $rules = [
                'country' => 'required',
                'district' => 'required',
                'town' => 'required',
                'altitude' => 'required|integer',
            ];
            
            $request->validate($rules);
            
            $values = Input::get();

            $id = $values['id'];

            $geocaching = $this->geocachingManager->getById($id);

            $geocaching->town = $values['town'];
            $geocaching->district = $values['district'];
            $geocaching->region = $values['region'];
            $geocaching->town = $values['town'];
            $geocaching->altitude = $values['altitude'];

            try{
                $this->geocachingManager->updateGeocaching($geocaching);
            }catch (Exception $e){

            }

            $request->session()->flash('message', 'Keška bola upravená');
            return response()->json(["redirect" => url("admin/geocaching")]);
        } else{
            
            $rules = [
                'locfile' => 'file|mimes:gpx'
            ];
            
            $values = Input::get();
            
            dd($request);
            
            
            $request->validate($rules);
            
            
            
        }
        
        
    }
 
}