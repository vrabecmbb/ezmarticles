<?php

namespace App\Http\Controllers\admin;

use Aginev\Datagrid\Datagrid;
use App\Entity\Diary;
use App\Http\Controllers\Controller;
use App\Managers\Dates;
use App\Managers\DiaryManager;
use App\Model\FormErrors;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Image;
use ZanySoft\Zip\Zip;

class DiaryController extends Controller
{
    const EDIT_MODE = 'edit';
    const INSERT_MODE = 'insert';

    /** @var Dates */
    private $dates;

    /** @var DiaryManager */
    private $diaryManager;

    /**
     * DiaryController constructor.
     * @param Application $app
     * @param Dates $dates
     * @param FormErrors $formErrors
     * @throws \ReflectionException
     */
    public function __construct(Application $app, Dates $dates, FormErrors $formErrors, DiaryManager $diaryManager)
    {
        $this->dates = $dates;
        $this->formErrors = $formErrors;
        $this->diaryManager = $diaryManager;
        parent::__construct($app);
    }

    public function index()
    {
        $grid = $this->createGrid();

        return view('pages.admin.diary.index', [
            'grid' => $grid
        ]);
    }

    public function edit($id)
    {
        $diary = $this->diaryManager->get($id);

        if (!$diary) {
            return redirect("admin/diary");
        }

        return view("pages.admin.diary.form", [
            "mode" => self::EDIT_MODE,
            "diary" => $this->prepareValues($diary),
            "thumbnails" => $this->preparePhotogallery($diary)
        ]);
    }

    private function preparePhotogallery($diary)
    {
        if (!$diary->photogallery)
            return [];

        $gallery = ltrim($diary->photogallery, '.');
        $ret = unserialize($gallery);

        return $ret;
    }

    public function create()
    {
        return view("pages.admin.diary.form", [
            "mode" => self::INSERT_MODE
        ]);
    }

    public function prepareValues($diary)
    {
        $ret = [
            'id' => $diary->id,
            'title' => $diary->title,
            'fromDate' => $this->dates->toFormatedString($diary->fromDate, Dates::SK),
            'toDate' => $diary->toDate ? ($this->dates->toFormatedString($diary->toDate, Dates::SK)) : "",
            'text' => $diary->text
        ];

        return $ret;
    }

    public function store(Request $request)
    {
        $data = Input::get();

        $rules = [
            'title' => 'required',
            'fromDate' => 'required|before:tomorrow',
            'toDate' => 'nullable|after:fromDate',
            'text' => 'required'
        ];

        $validator = Validator::make($data, $rules, $this->formErrors);

        $errors = $validator->errors();

        if ($errors->hasAny()) {
            return back()->withErrors($errors)->withInput($data);
        }

        $message = $this->processForm($request);

        $request->session()->flash('message', $message);

        return Redirect::route('diary.index');

    }

    /**
     * @param $request
     * @throws \Exception
     */
    public function processForm($request)
    {

        $data = $request->all();

        $mode = $data['mode'];

        $fromDate = \DateTime::createFromFormat(Dates::SK, $data['fromDate']);
        $fromDate->setTime(0, 0, 0);

        if (isset($data['toDate'])) {
            $toDate = \DateTime::createFromFormat(Dates::SK, $data['toDate']);
        }

        $fromDate = \DateTime::createFromFormat(Dates::SK, $data['fromDate']);
        $fromDate->setTime(0, 0, 0);

        $diary = $mode == self::INSERT_MODE ? new Diary() : $this->diaryManager->get($data['id']);

        $diary->title = $data['title'];
        $diary->fromDate = $fromDate;
        if (!empty($data['toDate'])) {
            $diary->toDate = $toDate;
        }
        $diary->text = $data['text'];

        /*$file = $data['file'];

        $file->store("pictures");*/

        //SAVE Diary
        $this->em->beginTransaction();

        if ($mode == self::INSERT_MODE) {
            $inserted_id = $this->diaryManager->insert($diary);
        } else {
            $this->diaryManager->update($diary);
            $inserted_id = $diary->id;
        }

        if (isset($data['file'])) {
            $file = $data['file'];

            $zip = Zip::open($file->path());
            $zip->extract(storage_path('diary_zip'));

            $files = preg_grep('~\.(jpeg|jpg|png)$~', scandir(storage_path("diary_zip")));

            foreach ($files as $file) {

                $fileName = substr($file, 0, strlen($file) - 4);
                $fileType = substr($file, strlen($file) - 3, strlen($file));

                $image_resizer = Image::make(storage_path("diary_zip/" . $file));

                if (!file_exists(storage_path('app/diary/' . $inserted_id))) {
                    mkdir(storage_path('app/diary/' . $inserted_id), 666, true);
                }

                $image_path = storage_path('app/diary/' . $inserted_id . '/' . $fileName . '.' . $fileType);
                $thumb_path = storage_path('app/diary/' . $inserted_id . '/' . $fileName . '_thumb.' . $fileType);

                $image_resizer->save($image_path);
                $image_resizer->resize(125, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $image_resizer->save($thumb_path);

                $imagesArray[] = [
                    'image' => 'diary/' . $inserted_id . '/' . $fileName . '.' . $fileType,
                    'thumb' => 'diary/' . $inserted_id . '/' . $fileName . '_thumb.' . $fileType
                ];
            }

            $this->diaryManager->addPhotogallery($inserted_id, serialize($imagesArray));
        }
        $this->em->commit();

        $message = self::EDIT_MODE ? 'Denník bol úspešne aktualizovaný' : 'Denník bol úspešne vytvorený';

        return $message;


    }

    public function createGrid()
    {
        $default_filter = [
            "order_by" => "id",
            "order_dir" => "DESC"
        ];

        $filter = Input::get('f') ? Input::get('f') : $default_filter;


        $repository = $this->em->getRepository(Diary::class);

        $paginator = $repository->getAllPage($filter);

        $results = $paginator->getCollection();

        $grid = new Datagrid($results, Input::get('f', []));

        $grid
            ->setColumn('id', 'ID', [
                'sortable' => true,
                'order' => 'DESC'
            ])
            ->setColumn('title', 'Nazov', [
                'sortable' => true
            ])
            ->setColumn('fromDate', 'Datum', [
                'sortable' => true
            ])
            ->setColumn('fromDate', 'Datum', [
                'sortable' => true,
                'wrapper' => function ($value, $row) {
                    $date = $row->getData()['fromDate.date'];
                    $datetime = new \Datetime($date);
                    return $this->dates->toFormatedString($datetime, Dates::SK);
                }
            ])
            ->setActionColumn([
                'wrapper' => function ($value, $row) {
                    return '<a href="' . action('Admin\DiaryController@edit', $row->id) . '">Up</a>';
                }
            ]);

        $grid->setPagination($paginator);

        return $grid;
    }
}
