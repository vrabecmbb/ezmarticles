<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 29. 10. 2018
 * Time: 22:50
 */

namespace App\Http\Controllers\Admin;


use Illuminate\Support\Facades\Auth;

class HomepageController
{
    public function index()
    {
        return view('pages.admin.index');
    }
}