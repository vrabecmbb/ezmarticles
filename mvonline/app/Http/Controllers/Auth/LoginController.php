<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 29. 10. 2018
 * Time: 20:01
 */

namespace App\Http\Controllers\Auth;

use App\Entity\Users;
use App\Http\Controllers\Controller;
use Doctrine\ORM\EntityManager;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;

class LoginController extends Controller{

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';


    public function login(Request $request){

        $errors = new MessageBag();

        $values = $request->only('login','password');

        $login = $values['login'];
        $password = $values['password'];

        $admin = $this->em->getRepository(Users::class)->findOneBy([
            'login' => $login
        ]);

        if (!$admin){
            //throw new \Exception("Používateľ $login nebol nájdený");
            $errors->add('error',"Používateľ $login nebol nájdený");
            return Redirect::back()->withInput()->withErrors($errors);
        }

        if ($admin->getPassword() !== $this->calculateHash($password)) {
            //throw new \Exception("Invalid password.");
            $errors->add('error',"Nespávne heslo");
            return Redirect::back()->withInput()->withErrors($errors);
        }


        Auth::login($admin);

        return redirect($this->redirectTo);
    }

    public function showLoginForm(){
        return view('auth.login');
    }

    private function calculateHash($string){
        return hash('sha256',$string);
    }

}