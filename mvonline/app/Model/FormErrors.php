<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 15. 11. 2018
 * Time: 7:11
 */

namespace App\Model;


class FormErrors
{
    public function getMessages(){

        $messages = [
            'title.required' => 'Nadpis musi byt vyplneny',
            'required' => 'Pole :attribute musi byt vyplnene',
            'fromDate.before' => 'Dátum (Od) musí byť pred zajtrajším',
            'toDate.after' => 'Datum konca musi byt po zaciatocnom'
        ];

        return $messages;
    }
}