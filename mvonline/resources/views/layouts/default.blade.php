<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Martin Vrabec">
    <meta name="robots" content="all,follow">
    <meta name="keywords" content="blog, portfolio, geocaching, martin, vrabec, mvtym, dennik, panorámy">
    <meta name="description" content="Osobný blog s denníkom">

    <title>@yield('title', 'Blog | mvonline.eu')</title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Maiden+Orange" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto+Slab&amp;subset=latin,latin-ext" rel="stylesheet"
          type="text/css">
    <link href="//fonts.googleapis.com/css?family=Exo+2&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Kelly+Slab&amp;subset=latin,latin-ext" rel="stylesheet"
          type="text/css">

    <link rel="shortcut icon" href="/images/favicon.ico">

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <!--script src="/js/main.js"></script-->

    <!-- FONT AVESOME -->
    <script src="/fontawesome/js/all.js"></script>
    <link rel="stylesheet" media="screen" href="/font-awesome/css/all.css">

    <!-- LIGHTBOX 2 -->
    <script src="/lightbox/js/lightbox.min.js"></script>
    <link href="/lightbox/css/lightbox.css" rel="stylesheet">

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" media="screen,projection,tv" href="{{asset("css/bootstrap.css")}}">
    <script src="{{asset("/js/bootstrap.min.js")}}"></script>

    <script src="{{asset("js/bootstrap-datepicker.js")}}"></script>

    <!-- HLAVNE CSS -->
    <link rel="stylesheet" media="screen,projection,tv" href="{{asset("css/style.css")}}">

    @yield("script")
</head>

<body>
<div id="wrapper">
    <header class="header">
        <div class="container">
            <img src="{{asset("images/mvonline.png")}}">
        </div>
    </header>
    <div class="slider-line"></div>
    <div id="content">
        <div class="container main">
            @if(Session::has('message'))
                <p class="alert alert-info">{{ Session::get('message') }}</p>
            @endif
            @yield('content')
        </div>
    </div>
</div>
<footer class="footer">

</footer>
</body>
</html>
