@extends('layouts.default')

@section('content')
    <div class="col-lg-12">
        <div class="grid-top-box">
            <div class="btn-group float-left">
                <button class="btn btn-default dropdown-toggle" type="button" data_toggle="dropdown">V3etko</button>
            </div>
            <div class="top-buttons btn-group float-right">
                <a href="{{action("Admin\DiaryController@create")}}" class="btn btn-success">Add</a>
            </div>
            {!! $grid->show() !!}
        </div>
    </div>
@endsection