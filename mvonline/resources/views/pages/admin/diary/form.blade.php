

@extends("layouts/default")

@section('script')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">

    <!-- Include Editor style. -->
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.0/css/froala_editor.pkgd.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.0/css/froala_style.min.css" rel="stylesheet"
          type="text/css"/>

    <!-- Include external JS libs. -->
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>

    <!-- Include Editor JS files. -->
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/npm/froala-editor@2.9.0/js/froala_editor.pkgd.min.js"></script>

    <!-- Bootstrap datepicker -->
    <script type="text/javascript" src="{{asset("datepicker/dist/js/bootstrap-datepicker.js")}}"></script>
    <script type="text/javascript" src="{{asset("datepicker/js/locales/bootstrap-datepicker.sk.js")}}"></script>
    <link href="{!! asset("datepicker/dist/css/bootstrap-datepicker.min.css") !!}" rel="stylesheet" type="text/css">

    <script type="text/javascript">
        $(function() {
            $(".datepicker").datepicker({
                format: 'dd.mm.yyyy',
                language: 'sk',
                startDate: '-2d'
            });

            $('.textarea').froalaEditor({
                width: '100%',
                heightMin: 250
            });
        });
    </script>
@endsection

@section("content")
    <div class="col-lg-12">
        <div class="top-buttons btn-group float-right">
            <a href="{{action("Admin\DiaryController@index")}}" class="btn btn-light">Spat</a>
        </div>
        {!! Form::open(['url' => "admin\diary", 'method' => 'post', 'file' => true,'enctype' => 'multipart/form-data']) !!}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-details form-details-bg clear">
            <div class="form-row">
                <div class="input-box size-b">
                    <span class="input-label">
                        {!! Form::label('title','Nadpis:', ['class' => 'control-label']) !!}
                    </span>
                    <div class="input-group">
                        {!! Form::text('title', $mode == \App\Http\Controllers\admin\DiaryController::EDIT_MODE ? ($diary['title']) : "", ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="input-box size-c">
                    <span class="input-label">
                        {!! Form::label('fromDate', 'Od:', ['class' => 'control-label datepicker']) !!}
                    </span>
                    <div class="input-group">
                        {!! Form::text('fromDate', $mode == \App\Http\Controllers\admin\DiaryController::EDIT_MODE ? $diary['fromDate'] : "", ['class' => 'form-control datepicker']) !!}
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="far fa-calendar-alt"></i></span>
                        </div>
                    </div>
                    {!! Form::hidden('mode', $mode) !!}
                </div>
                <div class="input-box size-c">
                    <span class="input-label">
                        {!! Form::label('toDate', 'Do:', ['class' => 'control-label']) !!}
                    </span>
                    <div class="input-group">
                        {!! Form::text('toDate', $mode == \App\Http\Controllers\admin\DiaryController::EDIT_MODE ? $diary['toDate'] : "", ['class' => 'form-control datepicker']) !!}
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="far fa-calendar-alt"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="input-box size-b">
                    <span class="input-label">
                        {!! Form::label('text', 'Text', ['class' => 'control-label']) !!}
                    </span>
                    <div class="input-group">
                        {!! Form::textarea('text', $mode == \App\Http\Controllers\admin\DiaryController::EDIT_MODE ? ($diary['text']) : "", ['class' => 'form-control textarea']) !!}
                    </div>
                </div>
            </div>
            <div class="form-row">
                 <span class="input-label">
                        {!! Form::label('keyword', 'Klucove slova', ['class' => 'control-label']) !!}
                    </span>
                <div class="input-group">
                    {!! Form::text('keyword', NULL, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-row">
                <div class="input-box size-b">
                    <span class="input-label">
                        {!! Form::label('file', 'Fotogaleria', ['class' => 'control-label']) !!}
                    </span>
                    <div class="input-group">
                        {!! Form::file('file') !!}
                    </div>
                    <div class="input-group">
                        <p class="help-block">ZIP subor s fotografiami</p>
                    </div>
                </div>
            </div>
            <div>
                @if ($mode == \App\Http\Controllers\admin\DiaryController::EDIT_MODE)
                    <div id="admin_thumbs">
                        <ul class="admin photogallery">
                            @foreach($thumbnails as $thumb)
                                <li><img src="{{\Illuminate\Support\Facades\Storage::url($thumb['thumb'])}}"></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            {!! Form::hidden('id', $mode == \App\Http\Controllers\admin\DiaryController::EDIT_MODE ? $diary['id'] : NULL) !!}
            <div class="form-row">
                <div class="input-box">
                    <div class="input-group">
                        {!! Form::submit('Submit', ['class' => 'form-control btn btn-success']) !!}
                        <a href="{{action("Admin\DiaryController@index")}}" class="btn btn-light">Spat</a>
                    </div>
                </div>
            </div>
        </div>

        {!! Form::close() !!}

    </div>
@endsection