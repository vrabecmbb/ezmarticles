{!! Form::open(['method' => 'post']) !!}
<div class="form-group">
    {!! Form::label('country', 'Krajina:', ['class' => 'control-label']) !!}
    {!! Form::text('country', 'Krajina', ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('region', 'Región:', ['class' => 'control-label']) !!}
    {!! Form::text('region', 'Región', ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('district', 'Okres:', ['class' => 'control-label']) !!}
    {!! Form::text('district', 'Okres', ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('town', 'Obec:', ['class' => 'control-label']) !!}
    {!! Form::text('town', 'Obec', ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('altitude', 'Nadm. výška:', ['class' => 'control-label']) !!}
    {!! Form::text('altitude', 'Nadm. výška', ['class' => 'form-control']) !!}
</div>
{!! Form::hidden('id','') !!}
{!! Form::submit('Submit', ['class' => 'd-none', 'id' => 'submit-btn']) !!}
{!! Form::close() !!}