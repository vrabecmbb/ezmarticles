{!! Form::open(['method' => 'post','files'=> true, 'id'=> 'processForm']) !!}
<div class="form-group">
    {!! Form::label('locfile', 'Súbor:', ['class' => 'control-label']) !!}
    {!! Form::file('locfile', []) !!}
</div>
<div class="form-group">
    {!! Form::checkbox('mapjson', 'Vytvoriť geojson:', true) !!}
    Vytvoriť geojson
</div>
{!! Form::submit('Submit', ['class' => 'd-none', 'id' => 'processsubmit-btn']) !!}
{!! Form::close() !!}