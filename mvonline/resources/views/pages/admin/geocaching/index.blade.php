@extends('layouts.default')

@section('content')
    <div class="col-lg-12">
        <div class="grid-top-box">
            <div class="btn-group float-left">
                <button class="btn btn-default dropdown-toggle" type="button" data_toggle="dropdown">Všetko</button>
            </div>
            <div class="top-buttons btn-group float-right">
                <a href="#" data-toggle="modal" data-target="#processmodal" class="btn btn-success">Pridať</a>
            </div>
            {!! $grid->show() !!}
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upraviť</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert-danger"></div>
                    @include('pages.admin.geocaching.editform')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvoriť</button>
                    <button type="button" id="btn-save" class="btn btn-primary">Uložiť</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal process form -->
    <div class="modal fade" id="processmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upraviť</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert-danger"></div>
                    @include('pages.admin.geocaching.addform')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvoriť</button>
                    <button type="button" id="process-btn-save" class="btn btn-primary">Uložiť</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.fillModal').click(function () {
                var id = $(this).attr('data-id');

                $.ajaxSetup({
                    headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
                });
                jQuery.ajax({
                    url: '/admin/geocaching/ajax/' + id,
                    type: 'GET',
                    success: function (data) {
                        $('input[name=altitude]').val(data.geocache.altitude);
                        $('input[name=country]').val(data.geocache.country.country);
                        $('input[name=town]').val(data.geocache.town);
                        $('input[name=district]').val(data.geocache.district);
                        $('input[name=region]').val(data.geocache.region);
                        $('input[name=id]').val(data.geocache.id);
                    }
                });
            });

            //edit form
            $('#btn-save').click(function () {

                    jQuery.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url: "{{ url('/admin/geocaching') }}",
                        method: 'post',
                        data: {
                            altitude: $('input[name=altitude]').val(),
                            country: $('input[name=country]').val(),
                            town: $('input[name=town]').val(),
                            district: $('input[name=district]').val(),
                            region: $('input[name=region]').val(),
                            id: $('input[name=id]').val(),
                            '_token':$('input[name=_token]').val()
                        },
                        success: function (data) {
                            window.location.href = data.redirect;
                        },
                        error: function (data) {
                            var errors = data.responseText;
                            var messages = JSON.parse(errors).errors;
                            $.each(messages, function (key, value) {
                                $('.alert-danger').show();
                                $('.alert-danger').append('<p>' + value + '</p>');
                            });
                        }

                    });
                });
        });
        
        $('#process-btn-save').click(function(){
            var form = $(this).parent().parent().find('form');
            
            form.trigger('submit');
               
        });
        
        $('#processForm').submit(function(e){
            
            e.preventDefault();
            
            var formData = new FormData(this);
                
                jQuery.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                
                jQuery.ajax({
                        url: "{{ url('/admin/geocaching') }}",
                        method: 'post',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            console.log(data);return;
                            //window.location.href = data.redirect;
                        },
                        error: function (data) {
                            var errors = data.responseText;
                            var messages = JSON.parse(errors).errors;
                            $.each(messages, function (key, value) {
                                $('.alert-danger').show();
                                $('.alert-danger').append('<p>' + value + '</p>');
                            });
                        }

                    });
                
            });

        
    </script>
@endsection