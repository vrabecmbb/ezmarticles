@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <section id="latest_diaries">
                <h2>Najnovšie denníky</h2>
                @foreach($diaries as $diary)
                    <div class="item">
                        <div class="head">
                            <div class="date_start text-right float-left">
                                <p class="day">{{$diary->fromDate->format('d')}}</p>
                                <p class="month">{{$diary->fromDate->format('m')}}</p>
                                <p class="year">{{$diary->fromDate->format('Y')}}</p>
                            </div>
                            <h3 class="float-left">{{$diary->title}}</h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="content">
                            {!! $diary->text !!}
                        </div>
                        <div class="photos">
                            @php
                                $photos = ltrim($diary->photogallery,'.');
                                $photos = unserialize($photos);
                            @endphp
                            @if ($photos !== NULL)
                                @foreach($photos as $photo)
                                    <a href="{{Storage::url($photo['image'])}}" data-lightbox="{{$diary->id}}"><img
                                                src="{{\Illuminate\Support\Facades\Storage::url($photo['thumb'])}}"></a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endforeach
            </section>
        </div>
        <div class="col-lg-6">
            <section id="latest_caches">
                <h2>Najnovšie kešky</h2>
                @foreach($geocaching as $item)
                    <div class="item">
                        <div class="head">
                            <div class="date_start text-right float-left" style="background: {{$item->type->color}};">
                                <p class="day">{{$item->found->format('d')}}</p>
                                <p class="month">{{$item->found->format('m')}}</p>
                                <p class="year">{{$item->found->format('Y')}}</p>
                            </div>
                            <div class="float-left gc_icon">
                                <img src="https://www.geocaching.com/images/wpttypes/{{$item->type->image}}" alt="">
                            </div>
                            <div class="float-left">
                                {{$item->name}}<br>
                                <small>{{$item->country->country}} | {{$item->region}} | {{$item->district}} | {{$item->town}}</small>
                            </div>
                        </div>
                    </div>
                @endforeach
            </section>
        </div>
    </div>
@endsection