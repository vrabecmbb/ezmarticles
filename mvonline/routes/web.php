<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index', function () {
    return view('pages.index');
});

Route::get('/admin', 'Admin\HomepageController@index', function () {
    return view('pages.admin.index');
})->middleware('auth');

Route::group(['prefix' => 'admin','middleware'=> 'auth'], function () {

    Route::resource('diary', 'Admin\DiaryController');
    Route::resource('geocaching','Admin\GeocachingController');
    
    
});

Route::get('/admin/geocaching/ajax/{id}','Admin\GeocachingController@ajax');

Auth::routes();


