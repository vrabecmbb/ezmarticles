<?php

namespace Controllers;

use App;

class ArticlesController {

    public function index() {
        
    }

    public function add() {
        if (!isset($_SESSION['loged_user'])) {
            return redirect('articles');
        }

        if ($_SESSION['loged_user']['role'] != 'admin') {
            return redirect('articles');
        }

        return view('article.form', ['user' => $_SESSION['loged_user']['id']]);
    }

    public function tag($id) {

        $tag = App::get('em')->find(\Models\Entities\Tag::class, $id);

        if (!$tag) {
            return redirect('articles');
        }


        return view('article.tag', [
            'tag' => $tag,
            'articles' => $tag->getArticles()
            ]
        );
    }

    public function processForm() {
        $title = $_POST['title'];
        $perex = $_POST['perex'];
        $text = $_POST['text'];
        $tags = $_POST['tags'];
        $image = $_FILES['image'];
        $publicationDate = $_POST['publication_date'];
        $user = $_POST['user'];

        if ($image) {
            $fileUploader = new \Libs\FileUploder();
            $fileUploader->setExtensions(["jpg", "gif", "png"]);
            $fileUploader->setFile($image);
            $image = $fileUploader->uploadFile("public/images/");
        }

        App::get('em')->getConnection()->beginTransaction();

        try {
            $article = new \Models\Entities\Article;
            $article->setTitle($title);
            $article->setPerex($perex);
            $date = \DateTime::createFromFormat("Y-m-d", $publicationDate);
            $article->setPublicationDate($date);
            $article->setText($text);
            $article->setUser(App::get('em')->find(\Models\Entities\User::class, $user));
            $article->setImage(isset($image) ? $image : NULL );

            App::get('em')->persist($article);
            App::get('em')->flush();

            $tagsManager = new \TagsManager();
            $tagsManager->processTags($tags, $article);

            App::get('em')->getConnection()->commit();
        } catch (\Exception $e) {
            App::get('em')->getConnection()->rollBack();
            printf("Error: %s", $e->getMessage());
        }
        //var_dump($article);
    }

    private function processTags($tags) {
        
    }

}
