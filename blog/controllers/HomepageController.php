<?php

namespace Controllers;

use App;

class HomepageController {

    public function index() {

        $date = new \DateTime;

        $queryBuilder = App::get('em')->createQueryBuilder();

        $queryBuilder
                ->select('a')
                ->from(\Models\Entities\Article::class, 'a')
                ->where('a.publicationDate <= :date')
                ->orderBy('a.publicationDate', 'DESC')
                ->setParameter('date', $date)
                ->setMaxResults(10)
        ;

        $articles = $queryBuilder->getQuery()->getResult();

        $data = [
            'articles' => $articles
        ];

        return view('index', $data);
    }

    public function signin() {
        if (empty($_SESSION['loged_user'])) {
            return view('loginForm');
        } else {
            redirect('');
        }
    }

    public function signout() {
        if (!empty($_SESSION['loged_user'])) {
            unset($_SESSION['loged_user']);
        }
        redirect('');
    }

    public function processLogin() {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $findUser = App::get('em')->getRepository(\Models\Entities\User::class)->findOneBy(['login' => $email, 'password' => hash('sha256', $password)]);
        if ($findUser) {

            $store = [
                'email' => $findUser->getLogin(),
                'role' => $findUser->getRole(),
                'id' => $findUser->getId()
            ];

            $_SESSION['loged_user'] = $store;

            redirect('');
        }
    }

}
