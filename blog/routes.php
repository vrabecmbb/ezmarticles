<?php

$router->get("","HomepageController@index");

//sign in
$router->get("signin","HomepageController@signin");
$router->post("processLogin","HomepageController@processLogin");

//sign out
$router->get("signout","HomepageController@signout");

//admin
$router->get("admin","AdminController@index");

//articles
$router->get("articles/add","ArticlesController@add");
$router->post("articles/process","ArticlesController@processForm");

//tag
$router->get("tag","ArticlesController@tag");