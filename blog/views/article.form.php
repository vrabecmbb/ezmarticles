<?php require 'views/include/header.php' ?>

<div class="col-lg-12">
    <div class="row">
        <form action="process" method="post" enctype="multipart/form-data">
            <input type="text" name="title" placeholder="Nadpis">
            <textarea name="perex" placeholder="Perex"></textarea>
            <textarea name="text" placeholder="Text"></textarea>
            <input type="text" name="tags" placeholder="Tagy">
            <input type="file" name="image">
            <input type="date" name="publication_date">
            <input type="hidden" name="user" value="<?= $user ?>">
            <input type="submit" value="Odoslať">
        </form>
    </div>
</div>    