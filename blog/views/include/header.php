<html lang="sk">
    <head>
	<meta charset="utf-8">
	<title>Articles</title>
        <link href="<?= HOMEPAGE?>/public/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= HOMEPAGE?>/public/css/style.css" rel="stylesheet">
        
        <link href="<?= HOMEPAGE?>/public/fontawesome/css/all.css" rel="stylesheet">
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <header>
                    <div class="container">
                        <?php require 'views/include/menu.php'; ?>
                    </div>
                </header>
            </div>
            <div id="content">
                <div class="container">