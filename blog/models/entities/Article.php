<?php


namespace Models\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class Article {
    
    public function __construct() {
        $this->comments = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;
    
    /**
     * @ORM\Column (name="title")
     */
    private $title;
    
    /**
     * @ORM\Column (type="text") 
     */
    private $perex;
    
    /**
     * @ORM\Column (type="text") 
     */
    private $text;
    
    /**
     * @ORM\Column (type="text", nullable=true) 
     */
    private $image;
    
    /**
     * One article has many comments
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="article")
     */
    private $comments;
    
    /**
     * Many articles have one user.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="articles")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * Many Articles have Many Tags.
     * @ORM\ManyToMany(targetEntity="Tag", mappedBy="articles")
     */
    private $tags;
    
    /**
     * @ORM\Column (type="datetime")
     */
    private $publicationDate;
    
    function getId() {
        return $this->id;
    }

    function getTitle() {
        return $this->title;
    }

    function getPerex() {
        return $this->perex;
    }

    function getText() {
        return $this->text;
    }

    function getImage() {
        return $this->image;
    }

    function getComments() {
        return $this->comments;
    }

    function getUser() {
        return $this->user;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setPerex($perex) {
        $this->perex = $perex;
    }

    function setText($text) {
        $this->text = $text;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setComments($comments) {
        $this->comments = $comments;
    }

    function setUser($user) {
        $this->user = $user;
    }
    
    function getPublicationDate() {
        return $this->publicationDate;
    }

    function setPublicationDate($publicationDate) {
        $this->publicationDate = $publicationDate;
    }
    
    function getTags() {
        return $this->tags;
    }

}
