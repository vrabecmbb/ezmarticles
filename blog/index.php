<?php

require_once "bootstrap.php";
require_once "config.php";

$uri = trim($_SERVER['REQUEST_URI'],'/'.APP_HOMEPAGE.'/');
$method = $_SERVER['REQUEST_METHOD'];

require Router::load('routes.php')
            ->direct($uri, $method)
;
